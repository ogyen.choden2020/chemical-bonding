# Team Member

[Home](./Index.md) | [Project](./Project_Idea.md)   |  [Team](./team.md)  |  [Terminologies](./terminologies.md)


# Tshering Wangmo: 
![](Images/Tshering.jpg)
Hii, I am Tshering Wangmo currently studying at The Royal Academy in grade 11. I love reading books, especially the fiction which drives me into the story. I am fond of listening to music and watching dramas and love learning new languages which helps me get along with the more people around.  I am part of making this project, because I want to learn in depth about these particular concepts and want to explore more. In this project, I am mostly involved in drawing, measuring and designing the model and I sometimes help in doing documentation. Finally I am looking forward to more exciting activities on the way of doing this project. 


# Deki Lhazom:
![](Images/deki.jpg)

Hello! I am Deki Lhazom, studying in the 11th grade. I enjoy keeping myself physically engaged in activities such as playing basketball, football, and I really enjoy running. In this project, my role is to  collect information about this concept and analyze it. And sometimes I help to feed information in documentation which I have analyzed by going through the concepts. Through this project, I hope to expand my knowledge in the field of technology, which I am not very proficient in. Moreover, I aim to improve my research skills, creativity, and analysis skills throughout the course of this project.

# Choeyang Yeshey Dema: 
![](Images/choeyang.jpg)

Hi, I'm Choeyang Yeshey Dema, studying in the eleventh grade. I love reading books, whether it's fiction or nonfiction. Books are like friends to me, guiding me through different worlds. Besides studying, I enjoy playing sports. It's not just fun but it also helps me think in a different perspective. In this Project, I am mostly involved in connecting circuits and I am also involved in designing. I'm excited to learn new things and I'm ready to tackle the challenges with determination.

# Ugyen Choden:
 ![](Images/ugyen.JPG)

Hi, I'm Ugyen Choden. I'm currently studying in 11th grade. I have many hobbies, but I particularly enjoy reading books, listening to music, and dancing. I'm working on this project because we believe it will help us understand concepts better through the model itself. And in this project my role is to do documentation about our journey in making this project. We think that juniors will grasp the concepts well through the model we create.

[Home](./Index.md) | [Project](./Project_Idea.md)   |  [Team](./team.md)  |  [Terminologies](./terminologies.md)


