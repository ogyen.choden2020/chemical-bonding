[Home](./Index.md) | [Project](./Project_Idea.md)   |  [Team](./team.md)  |  [Terminologies](./terminologies.md)

# Types of bonds:
**1**. **Ionic bond**: An ionic bond is like a give-and-take relationship between atoms. Imagine one atom has something the other wants, like an electron. So, it hands over the electron, making itself a bit positive, and the other atom becomes a bit negative because it took the electron. They stick together because opposites attract, just like magnets. This kind of bond usually happens between metals and non-metals, like sodium and chlorine in table salt. Sodium gives away its electron to chlorine, and they stick together because of this electric attraction, forming a strong bond.

**2**.**Coordinate bond**: A coordinate bond is like sharing a snack where one friend brings all the snacks and shares them with another friend who didn't have any. In this bond, one atom has extra electrons (the "snacks") and shares them with another atom that needs electrons to form a stable pair. So, instead of each bringing something to share, one friend is generous and provides everything. This creates a strong bond between the atoms, like a friendship formed through sharing.

**3**.**Covalent bond**: A covalent bond is like a shared secret between atoms. In this type of bond, atoms share pairs of electrons to fill up their outer shells and become stable. It's like two friends agreeing to keep a secret between them, where each electron pair acts as a secret they both share. Neither atom completely gives away its electrons; instead, they share them, forming a strong connection. Covalent bonds are common in molecules like water (H₂O) and methane (CH₄), where atoms share electrons to stay together and create stable compounds.

# Links to watch related to this concept: 

[![Video Preview](https://i.ytimg.com/vi/OTgpN62ou24/maxresdefault.jpg)](https://youtu.be/OTgpN62ou24?si=Y_YIZQaB3HANlwXj)
**To explore what is chemical bonding through video**

[![Video Preview](https://i.ytimg.com/vi/hBqzKrbcTFA/maxresdefault.jpg)](https://youtu.be/hBqzKrbcTFA?si=39i10DCFBSR2tdsV)
**To explore how ionic,coordinate and covalent bond works**

